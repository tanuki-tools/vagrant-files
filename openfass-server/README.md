# OpenFaaS Installation

## Introduction

There are several ways to install OpenFaaS:

- on Kubernetes: https://docs.openfaas.com/deployment/kubernetes/
- on Docker Swarm: https://docs.openfaas.com/deployment/docker-swarm/

there is an article explaining how to setup OpenFaaS on MiniKube:
https://medium.com/devopslinks/getting-started-with-openfaas-on-minikube-634502c7acdf


For my installation, I decided to install OpenFaaS (the Docker Swarm version) in a VM and to use a private Docker registry (which I use in another VM). So you need **Vagrant** and **VirtualBox**. I did that because I want to be sure to have a reproducible setup everywhere.

So I created a vagrant file (that you can find in this repository)

### Some important things to know about this `VagrantFile`

You can change the IP and name of the virtual machines in the `Vagrantfile`:

```ruby
REGISTRY_NAME = "my-registry"
REGISTRY_IP = "172.16.245.150"
REGISTRY_DOMAIN = "registry.test"
REGISTRY_PORT = 5000
REGISTRY = "registry.test:#{REGISTRY_PORT}"

OPENFAAS_NAME = "my-openfaas"
OPENFAAS_IP = "172.16.245.160"
OPENFAAS_PUB_IP = "192.168.1.160"
```

And in the OpenFaaS VM, I add the registry as an unsecure registry in `/etc/docker/daemon.json`

```ruby
# Add unsecure registry
echo "" >> /etc/docker/daemon.json
echo '{' >> /etc/docker/daemon.json
echo '  "insecure-registries" : ["#{REGISTRY}"]' >> /etc/docker/daemon.json
echo '}' >> /etc/docker/daemon.json
echo "" >> /etc/docker/daemon.json
```

## OpenFaaS and Docker registry install

You only need to copy the `Vagrantfile` in a directory, and run `vagrant up` at the root of the directory. And wait a little.

During the setup of **OpenFaaS**, if you watch the logs, you'll get the credentials of the **OpenFaaS** server. Something like that:

```
[Credentials]
 username: admin
 password: d3d24b12dcd369ef6707bc810c110b74b70cdfdd4e83f8b021c3d5e19d4ca86c
```

You need to update your `/etc/hosts` file with this data:

```
172.16.245.150 registry.test
172.16.245.160 open-faas.test
```

Now you can try to access to http://open-faas.test:8080 (and you need your credentials)

## CLI Installation

The documentation about the CLI installation is here: [https://github.com/openfaas/faas-cli#get-started-install-the-cli](https://github.com/openfaas/faas-cli#get-started-install-the-cli) or here: [https://docs.openfaas.com/cli/install/](https://docs.openfaas.com/cli/install/). In my case, I'm using **OSX**, so I just need to ype this:

```
brew install faas-cli
```

> and `brew upgrade faas-cli` if you want to update (and you are on **OSX**)

:warning: :information: On your laptop side (or desktop), you need to install **Docker** and in the `Daemon` panel of the **Docker** preferences, you need to add `registry.test:5000` as an insecure registry to be able to use it (then click on **Apply & Restart**).

Now you can write your first **OpenFaaS** function. And it's here: [https://gitlab.com/openfaas-experiments/openfaas-first-steps](https://gitlab.com/openfaas-experiments/openfaas-first-steps)

And/Or you can read these blogposts:

- [https://k33g.gitlab.io/articles/2018-10-20-OPENFAAS.html](https://k33g.gitlab.io/articles/2018-10-20-OPENFAAS.html)
- [https://k33g.gitlab.io/articles/2018-10-28-OPENFAAS.html](https://k33g.gitlab.io/articles/2018-10-28-OPENFAAS.html)
- [https://k33g.gitlab.io/articles/2018-11-11-OPENFAAS.html](https://k33g.gitlab.io/articles/2018-11-11-OPENFAAS.html)
